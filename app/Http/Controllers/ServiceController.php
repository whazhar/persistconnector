<?php

namespace App\Http\Controllers;
use DB;

class ServiceController extends Controller
{
    public function show($serviceNumber)
    {
        $serviceInfo = DB::select('select * from DWH_Activities where service_num = ?', [$serviceNumber]);
        
        return $serviceInfo;
    }
}
